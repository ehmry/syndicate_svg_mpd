# SPDX-FileCopyrightText: ☭ 2021 Emery Hemingway
# SPDX-License-Identifier: Unlicense

import std/[asyncdispatch, options, streams, strtabs, times, xmltree]
import preserves
import syndicate, syndicate/[actors, capabilities, relay]
  # TODO: move the exported Syndicate stuff to the top module
import nimsvg
import mpdclient

import svui
type Svui = svui.Svui[Ref]

proc generateSvg(mpd: MPDClient): string =
  var
    s = newStringStream()
    status = mpd.status
  if status.state in {statePlay, statePause}:
    if status.song.isSome:
      s.writeLine "song: " & $status.song.get.pos
      s.writeLine "elapsedTime: " & $(status.elapsed.inSeconds)
      s.writeLine "totalTime: " & $(status.duration.inSeconds)
    map(mpd.currentSong) do (song: Song):
      s.writeLine "uri: ", song.file
      if song.artists.len > 0:
        s.writeLine "artist: ", song.artists[0]
      s.writeLine "album: ", song.tags.getOrDefault $tagAlbum
      s.writeLine "title: ", song.title
      s.writeLine "track: ", song.tags.getOrDefault $tagTrack
      s.writeLine "name: ", song.name
      s.writeLine "date: ", song.tags.getOrDefault $tagDate
  else:
    s.writeLine "MPD inactive"
  s.setPosition 0
  var nodes = buildSvg:
    svg(width=400, height=300):
      g:
        text(`font-family` = "Verdana", x="30", y="30", `font-size`="20"):
          t: s.readAll.escape
  render nodes

when isMainModule:
  proc quitHook() {.noconv.} = quit()
  setControlCHook(quitHook)

  waitFor runActor("client") do (turn: var Turn):

    let
      cap =  mint()
      mpd = newMPDClient()
    var svuiHandle: Handle

    connectUnix(turn, "/run/syndicate/ds", cap) do (turn: var Turn; a: Assertion) -> TurnAction:
      let ds = unembed a

      svuiHandle = replace(turn, ds, svuiHandle, Svui(svg: generateSvg(mpd)))
        # TODO: poll MPD with idle messages,
        # not sure how to do that with the Syndicate Nim DSL
