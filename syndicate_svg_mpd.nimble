# Package

version       = "0.1.0"
author        = "Emery Hemingway"
description   = "Syndicate SVG generator for MPD"
license       = "Unlicense"
srcDir        = "src"
bin           = @["syndicate_svg_mpd"]


# Dependencies

requires "nim >= 1.6.0", "mpdclient", "svui"
